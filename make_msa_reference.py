#!/usr/bin/env python

from __future__ import print_function

import argparse
import json
from collections import OrderedDict
from inspect import getsourcefile
from os.path import abspath
import datetime
import sys

from astropy.io import fits
import numpy as np

def get_states(chk, sm, fs):
    ext_state = 'normal'
    int_state = 'normal'
    vignetted = 'no'

    if np.any(np.right_shift(sm, [4,5,6,7])) or np.any(np.right_shift(chk, [2,1])):
        ext_state = 'closed'
        int_state = 'closed'
    
    # FO overrides Short
    if np.any(np.right_shift(chk, [8,9])):
        ext_state = 'open'
        int_state = 'open'
        
    # vignetting overrides an 'open' ext_state
    if fs == 8:
        ext_state = 'closed'
        vignetted = 'yes'
    
    return ext_state, int_state, vignetted

def main(args):

    chk = fits.open(args.CHK)
    sm = fits.open(args.ShortMask)
    fs = fits.open(args.FieldStop)

    py3 = sys.version_info[0] > 2
    if py3:
        descript  = input("Reason for delivery: ")
    else:
        descript = raw_input("Reason for delivery: ")

    with open(args.output_name, "w") as f:
        f.write("{\n")
        f.write('"title": " MSA Shutter Operability File ",\n')
        f.write('"reftype" : "MSAOPER",\n')
        f.write('"pedigree": "GROUND",\n')
        f.write('"author": "M. Hill",\n')
        f.write('"telescope": "JWST",\n')
        f.write('"exp_type": "NRS_IFU|NRS_MSASPEC",\n')
        f.write('"instrument": "NIRSPEC",\n')
        f.write('"useafter": "2014-01-01T00:00:00",\n')
        f.write('"descript": "{}",\n'.format(descript))
        sourcefile = getsourcefile(lambda:0) # get the name of this file
        f.write('"HISTORY": "JSON file created with data provided by the NIRSpec team, '
            'as documented in ESA-JWST-SCI-NRS-TN-2016-001 and extracted from files '
            '{}, {}, {}, with code {}",\n'.format(args.CHK.split('/')[-1], args.ShortMask.split('/')[-1], args.FieldStop.split('/')[-1], sourcefile))

        f.write('"msaoper": [\n')
        msa_status = []
        for Q in range(1,5):
            for x in range(1, 366):
                for y in range(1, 172):
                    N = x + (y-1) * 365 - 1
                    chk_flag = chk[Q].data['STATUS'][N]
                    sm_flag = sm[Q].data['STATUS'][N]
                    fs_flag = fs[Q].data['STATUS'][N]
                    if chk_flag == 0 and sm_flag == 0 and fs_flag == 0:
                        continue
                    else:
                        ext_state, int_state, vignetted = get_states(chk_flag, sm_flag, fs_flag)
                        msa_row = '    {'
                        msa_row += '"Q": {},"x": {},"y": {},"state": "{}","TA state": "{}","Internal state": "{}","Vignetted": "{}"'.format(Q, x, y, ext_state, ext_state, int_state, vignetted)
                        msa_row += '}'
                        msa_status.append(msa_row)

        f.write(",\n".join(msa_status))
        f.write("\n]\n")

        f.write("}")

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description="Generate a JSON MSA Reference file.  The reason for delivery should be input when prompted and will be added to the \"descipt\" keyword of the generated reference file",
        epilog='example: python make_msa_reference.py listMSA_CHK_20151211.msl.zip listMSA_ShortMask_20160324.mls.zip listMSA_FieldStop_20160106.msl.zip output.json')
    parser.add_argument('CHK', help='CHK reference file')
    parser.add_argument('ShortMask', help='ShortMask reference file')
    parser.add_argument('FieldStop', help='FieldStop reference file')
    parser.add_argument('output_name', help='path for JSON output')
    args = parser.parse_args()
    main(args)